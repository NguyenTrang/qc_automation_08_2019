<?php

use Page\Acceptance\LoginApi as Login;
use Step\Acceptance\LoginApi as LoginApi;
class LoginCest
{

	public function __construct()
	{
		$this->username = 'laithihongthom@gmail.com';

		$this->password = 'Abc@123456';
	}

	/**
	 * @param ApiTester $I
	 * @throws Exception
	 */
	public function _before(LoginApi $I)
	{
		$I->haveHttpHeader('Content-Type', 'application/x-www-form-urlencoded');
		$I->login($this->username, $this->password);
		$I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
		$results = $I->grabDataFromResponseByJsonPath('$.result');
		$token =  $results[0]['token'];

		$I->comment($token);

		$I->haveHttpHeader('Authorization', 'x-access-token '.$token);
		$I->sendGET(Login::$URLAllUser);
		$I->seeResponseIsJson();
		$results = $I->grabDataFromResponseByJsonPath('$.result');


		$token =  $results[0]['record'][0]['id'];

		$I->comment($token);


		$allValue = $I->grabResponse();
		$I->comment($allValue[3]);

	}

	// tests
	public function tryToTest(ApiTester $I)
	{
	}
}
