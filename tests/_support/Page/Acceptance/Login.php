<?php
namespace Page\Acceptance;

class Login
{
	// include url of current page
	public static $URL = '/administrator';


	public static $username = "//*[@id=\"mod-login-username\"]";

	public static $password = "//*[@id=\"mod-login-password\"]";

	public static $login = '//*[@id="form-login"]/fieldset/div[3]/div/div/button';

	public static $controlPage = ' Control Panel';

}
