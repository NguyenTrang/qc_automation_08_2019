<?php
namespace Step\Acceptance;
use Page\Acceptance\LoginApi as Login;
class LoginApi extends \ApiTester
{
	/**
	 * @param $user
	 * @param $password
	 * @throws \Exception
	 */
	public function login($user, $password)
	{
		$client = $this;
		$client->sendPOST(Login::$URL, [Login::$userName => $user, Login::$password => $password]);
	}
}